import {
  BaseBuildSystem, BuildSystem, CMakeBuildSystem, ChangeEvent, GnuBuildSystem, PyprojectBuildSystem, SetState, SystemKind
} from '@/lib/types';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { useCallback, useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import CloseIcon from '@mui/icons-material/Close';
import Dialog from '@mui/material/Dialog';
import EditIcon from '@mui/icons-material/Edit';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Unstable_Grid2';
import IconButton from '@mui/material/IconButton';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import VariablesInput from '@/components/inputs/VariablesInput';

type InputProps = {
  buildSystem: BuildSystem,
  setBuildSystem: SetState<BuildSystem>,
}

type FormProps = InputProps & {
  open: boolean,
  setOpen: SetState<boolean>,
}

const buildSystems = ["gnu", "cmake", "pyproject", "haskell"];

const cmakeBuildTypes = ["Release", "Debug", "RelWithDebInfo", "MinSizeRel"];

function Form({
  open, setOpen, buildSystem, setBuildSystem
}: FormProps) {
  const [kind, setKind] = useState<SystemKind>(buildSystem.kind);
  const [tests, setTests] = useState(buildSystem.tests);
  const [testTarget, setTestTarget] = useState(buildSystem.testTarget);
  const [variables, setVariables] = useState<Array<string>>(buildSystem.variables);

  const [buildType, setBuildType] = useState("Release");

  const setFromBuildSystem = useCallback(() => {
    setKind(buildSystem.kind);
    setTests(buildSystem.tests);
    setTestTarget(buildSystem.testTarget);
    if (buildSystem.kind === 'cmake') {
      setBuildType(buildSystem.buildType);
    } else {
      setBuildType("Release");
    }
    setVariables(buildSystem.variables);
  }, [buildSystem, setKind, setTests, setTestTarget, setBuildType, setVariables]);

  const handleClose = useCallback(() => {
    setFromBuildSystem();
    setOpen(false);
  }, [setFromBuildSystem, setOpen]);

  useEffect(setFromBuildSystem, [setFromBuildSystem]);

  const save = () => {
    const base: BaseBuildSystem = {
      kind,
      tests,
      testTarget,
      variables,
    };

    if (kind === "cmake") {
      const cmake: CMakeBuildSystem = {
        ...base,
        kind: "cmake",
        buildType,
      };
      setBuildSystem(cmake);
    } else if (kind === "pyproject") {
      const python: PyprojectBuildSystem = {
        ...base,
        kind: "pyproject",
      };
      setBuildSystem(python);
    } else {
      setBuildSystem(base as GnuBuildSystem);
    }
    setOpen(false);
  }

  const onChangeKind = useCallback((event: SelectChangeEvent) =>
    setKind(event.target.value as SystemKind), [setKind]);

  const onChangeTests = useCallback((event: ChangeEvent) =>
    setTests(event.target.checked), [setTests]);

  const onChangeTestTarget = useCallback((event: ChangeEvent) =>
    setTestTarget(event.target.value), [setTestTarget]);

  const onChangeBuildType = useCallback((event: SelectChangeEvent) =>
    setBuildType(event.target.value), [setBuildType]);

  return (
    <Dialog open={open} onClose={handleClose} fullScreen>
      <Grid container sx={{m:2}}>
        <Grid sm={6}>
          <Typography sx={{ flex: 1 }} variant="h4" component="div">
            Edit build system
          </Typography>
        </Grid>
        <Grid sm={6} display="flex" justifyContent="flex-end">
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
        </Grid>
        <Grid sm={12}>
          <FormControl variant="standard" sx={{mb: 2}}>
            <InputLabel>Kind</InputLabel>
            <Select
              label="Kind"
              value={kind}
              onChange={onChangeKind}
            >
              {buildSystems.map(value => (
                <MenuItem value={value} key={value}>{value}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid sm={12}>
          <FormControlLabel control={<Checkbox
            checked={tests}
            onChange={onChangeTests}
          />} label="Run tests" />
        </Grid>
        <Grid sm={12}>
          <FormControl fullWidth sx={{mb: 2, mt: 2}}>
            <TextField
              label="Tests target"
              value={testTarget}
              onChange={onChangeTestTarget}
            />
          </FormControl>
        </Grid>
        {kind === "cmake" && (
          <Grid sm={12}>
            <FormControl fullWidth sx={{mb: 2}}>
              <InputLabel>CMake Build Type</InputLabel>
              <Select
                label="CMake Build Type"
                value={buildType}
                onChange={onChangeBuildType}
              >
                {cmakeBuildTypes.map(value => (
                  <MenuItem value={value} key={value}>{value}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        )}
        <Grid sm={12}>
          <VariablesInput variables={variables} setVariables={setVariables} />
        </Grid>
        <Grid sm={12}>
          <Button onClick={save} variant="contained">Save</Button>
          <Button onClick={handleClose} sx={{ml: 5}}>Cancel</Button>
        </Grid>
      </Grid>
    </Dialog>
  );
}

export default function BuildSystemInput({
  buildSystem,
  setBuildSystem
}: InputProps) {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button
        fullWidth
        variant="contained"
        startIcon={<EditIcon />}
        onClick={() => setOpen(true)}
      >
        Customize build system and configuration
      </Button>
      <Form
        open={open}
        setOpen={setOpen}
        buildSystem={buildSystem}
        setBuildSystem={setBuildSystem}
      />
    </>
  );
}
